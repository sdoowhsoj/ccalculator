//
//  AboutViewController.h
//  CCalc
//
//  Created by Josh Woods on 3/11/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMenuItem.h"

@interface AboutViewController : UIViewController

@property (nonatomic, weak) IBOutlet UILabel *itemNameLabel;
@property (nonatomic, weak) IBOutlet UILabel *calLabel;
@property (nonatomic, weak) IBOutlet UILabel *tFatLabel;
@property (nonatomic, weak) IBOutlet UILabel *sfLabel;
@property (nonatomic, weak) IBOutlet UILabel *transfLabel;
@property (nonatomic, weak) IBOutlet UILabel *choLabel;
@property (nonatomic, weak) IBOutlet UILabel *sodLabel;
@property (nonatomic, weak) IBOutlet UILabel *tcLabel;
@property (nonatomic, weak) IBOutlet UILabel *dfLabel;
@property (nonatomic, weak) IBOutlet UILabel *sugLabel;
@property (nonatomic, weak) IBOutlet UILabel *proLabel;

@property (nonatomic, weak) IBOutlet UILabel *totalFatPercentLabel;
@property (nonatomic, weak) IBOutlet UILabel *saturatedFatPercentLabel;
@property (nonatomic, weak) IBOutlet UILabel *cholesterolPercentLabel;
@property (nonatomic, weak) IBOutlet UILabel *sodiumPercentLabel;
@property (nonatomic, weak) IBOutlet UILabel *carbsPercentLabel;
@property (nonatomic, weak) IBOutlet UILabel *fiberPercentLabel;

@property (nonatomic, strong) CCMenuItem *aboutMenuItem;

@end

//
//  FourthViewController.m
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import "FourthViewController.h"
#import "FifthViewController.h"
#import "AboutViewController.h"

@interface FourthViewController ()

@end

@implementation FourthViewController
{
    int pBeansServing;
    int bBeansServing;
    int wRiceServing;
    int bRiceServing;
    int fajitasServing;
}

-(IBAction)fajitasPressed
{
    if([self.fajitasAddButton isTouchInside])
    {
        self.fajitasBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        fajitasServing++;
        [self.menuItem addIngredientItem:self.fajitas];
        NSLog(@"%i", fajitasServing);
        if(fajitasServing >= 1){
            self.fajitasSubtractButton.hidden = NO;
        }
        self.fajitasSS.text = [NSString stringWithFormat:@"%i", fajitasServing];
        NSLog(@"%ld", (long)self.fajitas.nutrition.cal);
    } else if ([self.fajitasSubtractButton isTouchInside]){
        fajitasServing--;
        [self.menuItem removeIngredientItem:self.fajitas];
        self.fajitasSS.text = [NSString stringWithFormat:@"%i", fajitasServing];
        if(fajitasServing < 1)
        {
            self.fajitasBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.fajitasSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)wRicePressed
{
    if([self.wRiceAddButton isTouchInside])
    {
        self.wRiceBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        wRiceServing++;
        [self.menuItem addIngredientItem:self.wRice];
        NSLog(@"%i", wRiceServing);
        if(wRiceServing >= 1){
            self.wRiceSubtractButton.hidden = NO;
        }
        self.wRiceSS.text = [NSString stringWithFormat:@"%i", wRiceServing];
        NSLog(@"%ld", (long)self.wRice.nutrition.cal);
    } else if ([self.wRiceSubtractButton isTouchInside]){
        wRiceServing--;
        [self.menuItem removeIngredientItem:self.wRice];
        self.wRiceSS.text = [NSString stringWithFormat:@"%i", wRiceServing];
        if(wRiceServing < 1)
        {
            self.wRiceBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.wRiceSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)bRicePressed
{
    if([self.bRiceAddButton isTouchInside])
    {
        self.bRiceBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        bRiceServing++;
        [self.menuItem addIngredientItem:self.bRice];
        NSLog(@"%i", bRiceServing);
        if(bRiceServing >= 1){
            self.bRiceSubtractButton.hidden = NO;
        }
        self.bRiceSS.text = [NSString stringWithFormat:@"%i", bRiceServing];
        NSLog(@"%ld", (long)self.bRice.nutrition.cal);
    } else if ([self.bRiceSubtractButton isTouchInside]){
        bRiceServing--;
        [self.menuItem removeIngredientItem:self.bRice];
        self.bRiceSS.text = [NSString stringWithFormat:@"%i", bRiceServing];
        if(bRiceServing < 1)
        {
            self.bRiceBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.bRiceSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)pBeansPressed
{
    if([self.pBeansAddButton isTouchInside])
    {
        self.pBeansBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        pBeansServing++;
        [self.menuItem addIngredientItem:self.pBeans];
        NSLog(@"%i", pBeansServing);
        if(pBeansServing >= 1){
            self.pBeansSubtractButton.hidden = NO;
        }
        self.pBeansSS.text = [NSString stringWithFormat:@"%i", pBeansServing];
        NSLog(@"%ld", (long)self.pBeans.nutrition.cal);
    } else if ([self.pBeansSubtractButton isTouchInside]){
        pBeansServing--;
        [self.menuItem removeIngredientItem:self.pBeans];
        self.pBeansSS.text = [NSString stringWithFormat:@"%i", pBeansServing];
        if(pBeansServing < 1)
        {
            self.pBeansBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.pBeansSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)bBeansPressed
{
    if([self.bBeansAddButton isTouchInside])
    {
        self.bBeansBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        bBeansServing++;
        [self.menuItem addIngredientItem:self.bBeans];
        NSLog(@"%i", bBeansServing);
        if(bBeansServing >= 1){
            self.bBeansSubtractButton.hidden = NO;
        }
        self.bBeansSS.text = [NSString stringWithFormat:@"%i", bBeansServing];
        NSLog(@"%ld", (long)self.bBeans.nutrition.cal);
    } else if ([self.bBeansSubtractButton isTouchInside]){
        bBeansServing--;
        [self.menuItem removeIngredientItem:self.bBeans];
        self.bBeansSS.text = [NSString stringWithFormat:@"%i", bBeansServing];
        if(bBeansServing < 1)
        {
            self.bBeansBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.bBeansSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)goBack
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)startOverButtonPressed
{
    [self.menuItem.items removeAllObjects];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.aboutMenuItem = [[CCMenuItem alloc] init];
    
    self.fajitas = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeFajitas];
    self.wRice = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeWRice];
    self.bRice = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeBRice];
    self.pBeans = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypePBeans];
    self.bBeans = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeBBeans];
    
    self.fajitasInfoButton.tag = CCIngredientItemTypeFajitas;
    self.wRiceInfoButton.tag = CCIngredientItemTypeWRice;
    self.bRiceInfoButton.tag = CCIngredientItemTypeBRice;
    self.pBeansInfoButton.tag = CCIngredientItemTypePBeans;
    self.bBeansInfoButton.tag = CCIngredientItemTypeBBeans;
    
    self.fajitasSubtractButton.hidden = YES;
    self.wRiceSubtractButton.hidden = YES;
    self.bRiceSubtractButton.hidden = YES;
    self.pBeansSubtractButton.hidden = YES;
    self.bBeansSubtractButton.hidden = YES;
    
    NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
	FifthViewController *transferViewController = segue.destinationViewController;
    
	AboutViewController *aboutView = segue.destinationViewController;
	
	NSLog(@"prepareForSegue: %@", segue.identifier);
    
	if([segue.identifier isEqualToString:@"fourthSegue"])
	{
		transferViewController.menuItem = self.menuItem;
		NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
	} else if ([segue.identifier isEqualToString:@"fajitasAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"wRiceAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"bRiceAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"bBeansAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"pBeansAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	}
}

@end

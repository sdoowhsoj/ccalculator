//
//  FourthViewController.h
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMenuItem.h"

@interface FourthViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *fajitasInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *wRiceInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *bRiceInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *pBeansInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *bBeansInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *startOver;

@property (weak, nonatomic) IBOutlet UIButton *fajitasAddButton;
@property (weak, nonatomic) IBOutlet UIButton *fajitasSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *wRiceAddButton;
@property (weak, nonatomic) IBOutlet UIButton *wRiceSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *bRiceAddButton;
@property (weak, nonatomic) IBOutlet UIButton *bRiceSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *bBeansAddButton;
@property (weak, nonatomic) IBOutlet UIButton *bBeansSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *pBeansAddButton;
@property (weak, nonatomic) IBOutlet UIButton *pBeansSubtractButton;

@property (nonatomic, weak) IBOutlet UILabel *fajitasSS;
@property (nonatomic, weak) IBOutlet UILabel *wRiceSS;
@property (nonatomic, weak) IBOutlet UILabel *bRiceSS;
@property (nonatomic, weak) IBOutlet UILabel *pBeansSS;
@property (nonatomic, weak) IBOutlet UILabel *bBeansSS;
@property (weak, nonatomic) IBOutlet UILabel *fajitasBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *wRiceBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *bRiceBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *pBeansBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *bBeansBGLabel;

@property (nonatomic, strong) CCMenuItem *menuItem;
@property (nonatomic, strong) CCMenuItem *aboutMenuItem;
@property (nonatomic, strong) CCIngredientItem *fajitas;
@property (nonatomic, strong) CCIngredientItem *wRice;
@property (nonatomic, strong) CCIngredientItem *bRice;
@property (nonatomic, strong) CCIngredientItem *pBeans;
@property (nonatomic, strong) CCIngredientItem *bBeans;

@end

//
//  SecondViewController.h
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMenuItem.h"

@interface SecondViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *steakInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *chickenInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *barbacoaInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *carnitasInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *startOverButton;

@property (weak, nonatomic) IBOutlet UIButton *steakAddButton;
@property (weak, nonatomic) IBOutlet UIButton *steakSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *chickenAddButton;
@property (weak, nonatomic) IBOutlet UIButton *chickenSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *barbacoaAddButton;
@property (weak, nonatomic) IBOutlet UIButton *barbacoaSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *carnitasAddButton;
@property (weak, nonatomic) IBOutlet UIButton *carnitasSubtractButton;

@property (nonatomic, weak) IBOutlet UILabel *steakSS;
@property (nonatomic, weak) IBOutlet UILabel *chickenSS;
@property (nonatomic, weak) IBOutlet UILabel *carnitasSS;
@property (nonatomic, weak) IBOutlet UILabel *barbacoaSS;
@property (weak, nonatomic) IBOutlet UILabel *steakBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *chickenBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *carnitasBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *barbacoaBGLabel;

@property (nonatomic, strong) CCMenuItem *menuItem;
@property (nonatomic, strong) CCMenuItem *aboutMenuItem;

@property (nonatomic, strong) CCIngredientItem *steak;
@property (nonatomic, strong) CCIngredientItem *chicken;
@property (nonatomic, strong) CCIngredientItem *carnitas;
@property (nonatomic, strong) CCIngredientItem *barbacoa;

@end

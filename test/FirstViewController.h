//
//  ViewController.h
//  CCalc
//
//  Created by Josh Woods on 2/9/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMenuItem.h"

@interface FirstViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *burritoInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *bowlInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *softTacoInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *hardInfoTacoButton;
@property (weak, nonatomic) IBOutlet UIButton *startOver;

@property (weak, nonatomic) IBOutlet UIButton *burritoAddButton;
@property (weak, nonatomic) IBOutlet UIButton *burritoSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *bowlAddButton;
@property (weak, nonatomic) IBOutlet UIButton *bowlSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *hardAddButton;
@property (weak, nonatomic) IBOutlet UIButton *hardSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *softAddButton;
@property (weak, nonatomic) IBOutlet UIButton *softSubtractButton;

@property (nonatomic, weak) IBOutlet UILabel *burritoSS;
@property (nonatomic, weak) IBOutlet UILabel *bowlSS;
@property (nonatomic, weak) IBOutlet UILabel *softSS;
@property (nonatomic, weak) IBOutlet UILabel *hardSS;
@property (weak, nonatomic) IBOutlet UILabel *burritoBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *bowlBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *softBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *hardBGLabel;

@property (nonatomic, strong) CCMenuItem *menuItem;
@property (nonatomic, strong) CCMenuItem *aboutMenuItem;
@property (nonatomic, strong) CCIngredientItem *burrito;
@property (nonatomic, strong) CCIngredientItem *bowl;
@property (nonatomic, strong) CCIngredientItem *softTaco;
@property (nonatomic, strong) CCIngredientItem *hardTaco;

@end

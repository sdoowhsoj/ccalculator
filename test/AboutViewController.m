//
//  AboutViewController.m
//  CCalc
//
//  Created by Josh Woods on 3/11/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import "AboutViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

-(void)setNutritionLabels
{
    self.itemNameLabel.text = [NSString stringWithFormat:@"%@", self.aboutMenuItem.aboutNutrition.name];
    self.calLabel.text = [NSString stringWithFormat:@"%ld", (long)self.aboutMenuItem.aboutNutrition.cal];
    self.tFatLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.tFat];
    self.sfLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.sf];
    self.transfLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.transf];
    self.choLabel.text = [NSString stringWithFormat:@"%ld mg", (long)self.aboutMenuItem.aboutNutrition.cho];
    self.sodLabel.text = [NSString stringWithFormat:@"%ld mg", (long)self.aboutMenuItem.aboutNutrition.sod];
    self.tcLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.tc];
    self.dfLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.df];
    self.sugLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.sug];
    self.proLabel.text = [NSString stringWithFormat:@"%ld g", (long)self.aboutMenuItem.aboutNutrition.pro];
}

-(void)setDailyValueLabels
{
    self.totalFatPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.tFat*100)/65)];
    self.saturatedFatPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.sf*100)/20)];
    self.cholesterolPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.cho*100)/300)];
    self.sodiumPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.sod*100)/2400)];
    self.carbsPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.tc*100)/300)];
    self.fiberPercentLabel.text = [NSString stringWithFormat:@"%ld%%", ((self.aboutMenuItem.aboutNutrition.df*100)/25)];
}

-(void)closeAbout
{
    [self.aboutMenuItem.aboutItem removeAllObjects];
    [self.presentingViewController dismissViewControllerAnimated:YES completion:nil];
    NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    CCNutrition *nutrition = [CCNutrition new];
    nutrition = self.aboutMenuItem.aboutNutrition;
    
    [self setNutritionLabels];
    [self setDailyValueLabels];
    
    NSLog(@"%ld", (long)self.aboutMenuItem.aboutNutrition.cal);
    NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

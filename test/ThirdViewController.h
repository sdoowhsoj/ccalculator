//
//  ThirdViewController.h
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CCMenuItem.h"

@interface ThirdViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *tomatoInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *tRedInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *tGreenInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *chiliInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *startOver;

@property (weak, nonatomic) IBOutlet UIButton *tomatoAddButton;
@property (weak, nonatomic) IBOutlet UIButton *tomatoSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *tRedAddButton;
@property (weak, nonatomic) IBOutlet UIButton *tRedSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *chiliAddButton;
@property (weak, nonatomic) IBOutlet UIButton *chiliSubtractButton;
@property (weak, nonatomic) IBOutlet UIButton *tGreenAddButton;
@property (weak, nonatomic) IBOutlet UIButton *tGreenSubtractButton;

@property (nonatomic, weak) IBOutlet UILabel *tomatoSS;
@property (nonatomic, weak) IBOutlet UILabel *tRedSS;
@property (nonatomic, weak) IBOutlet UILabel *tGreenSS;
@property (nonatomic, weak) IBOutlet UILabel *chiliSS;
@property (weak, nonatomic) IBOutlet UILabel *tomatoBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *tRedBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *tGreenBGLabel;
@property (weak, nonatomic) IBOutlet UILabel *chiliBGLabel;

@property (nonatomic, strong) CCMenuItem *menuItem;
@property (nonatomic, strong) CCMenuItem *aboutMenuItem;
@property (nonatomic, strong) CCIngredientItem *tomato;
@property (nonatomic, strong) CCIngredientItem *tRed;
@property (nonatomic, strong) CCIngredientItem *tGreen;
@property (nonatomic, strong) CCIngredientItem *chili;

@end

//
//  ThirdViewController.m
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import "ThirdViewController.h"
#import "FourthViewController.h"
#import "AboutViewController.h"

@interface ThirdViewController ()

@end

@implementation ThirdViewController
{
    int tomatoServing;
    int tRedServing;
    int tGreenServing;
    int chiliServing;
}

-(IBAction)tomatoPressed
{
    if([self.tomatoAddButton isTouchInside])
    {
        self.tomatoBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        tomatoServing++;
        [self.menuItem addIngredientItem:self.tomato];
        NSLog(@"%i", tomatoServing);
        if(tomatoServing >= 1){
            self.tomatoSubtractButton.hidden = NO;
        }
        self.tomatoSS.text = [NSString stringWithFormat:@"%i", tomatoServing];
        NSLog(@"%ld", (long)self.tomato.nutrition.cal);
    } else if ([self.tomatoSubtractButton isTouchInside]){
        tomatoServing--;
        [self.menuItem removeIngredientItem:self.tomato];
        self.tomatoSS.text = [NSString stringWithFormat:@"%i", tomatoServing];
        if(tomatoServing < 1)
        {
            self.tomatoBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.tomatoSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)tRedPressed
{
    if([self.tRedAddButton isTouchInside])
    {
        self.tRedBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        tRedServing++;
        [self.menuItem addIngredientItem:self.tRed];
        NSLog(@"%i", tRedServing);
        if(tRedServing >= 1){
            self.tRedSubtractButton.hidden = NO;
        }
        self.tRedSS.text = [NSString stringWithFormat:@"%i", tRedServing];
        NSLog(@"%ld", (long)self.tRed.nutrition.cal);
    } else if ([self.tRedSubtractButton isTouchInside]){
        tRedServing--;
        [self.menuItem removeIngredientItem:self.tRed];
        self.tRedSS.text = [NSString stringWithFormat:@"%i", tRedServing];
        if(tRedServing < 1)
        {
            self.tRedBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.tRedSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)tGreenPressed
{
    if([self.tGreenAddButton isTouchInside])
    {
        self.tGreenBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        tGreenServing++;
        [self.menuItem addIngredientItem:self.tGreen];
        NSLog(@"%i", tGreenServing);
        if(tGreenServing >= 1){
            self.tGreenSubtractButton.hidden = NO;
        }
        self.tGreenSS.text = [NSString stringWithFormat:@"%i", tGreenServing];
        NSLog(@"%ld", (long)self.tGreen.nutrition.cal);
    } else if ([self.tGreenSubtractButton isTouchInside]){
        tGreenServing--;
        [self.menuItem removeIngredientItem:self.tGreen];
        self.tGreenSS.text = [NSString stringWithFormat:@"%i", tGreenServing];
        if(tGreenServing < 1)
        {
            self.tGreenBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.tGreenSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)chiliPressed
{
    if([self.chiliAddButton isTouchInside])
    {
        self.chiliBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        chiliServing++;
        [self.menuItem addIngredientItem:self.chili];
        NSLog(@"%i", chiliServing);
        if(chiliServing >= 1){
            self.chiliSubtractButton.hidden = NO;
        }
        self.chiliSS.text = [NSString stringWithFormat:@"%i", chiliServing];
        NSLog(@"%ld", (long)self.chili.nutrition.cal);
    } else if ([self.chiliSubtractButton isTouchInside]){
        chiliServing--;
        [self.menuItem removeIngredientItem:self.chili];
        self.chiliSS.text = [NSString stringWithFormat:@"%i", chiliServing];
        if(chiliServing < 1)
        {
            self.chiliBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.chiliSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)goBack
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)startOverButtonPressed
{
    [self.menuItem.items removeAllObjects];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
    
- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    self.aboutMenuItem = [[CCMenuItem alloc] init];
    
    self.tomato = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeTomato];
    self.tRed = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeTRed];
    self.tGreen = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeTGreen];
    self.chili = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeChili];
    
    self.tomatoInfoButton.tag = CCIngredientItemTypeTomato;
    self.tRedInfoButton.tag = CCIngredientItemTypeTRed;
    self.tGreenInfoButton.tag = CCIngredientItemTypeTGreen;
    self.chiliInfoButton.tag = CCIngredientItemTypeChili;
    
    self.tomatoSubtractButton.hidden = YES;
    self.tRedSubtractButton.hidden = YES;
    self.tGreenSubtractButton.hidden = YES;
    self.chiliSubtractButton.hidden = YES;
    
    NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
	FourthViewController *transferViewController = segue.destinationViewController;
    
    AboutViewController *aboutView = segue.destinationViewController;
	
	NSLog(@"prepareForSegue: %@", segue.identifier);
    
	if([segue.identifier isEqualToString:@"thirdSegue"])
	{
		transferViewController.menuItem = self.menuItem;
		NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
	} else if ([segue.identifier isEqualToString:@"tomatoAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"tRedAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"tGreenAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	} else if ([segue.identifier isEqualToString:@"chiliAboutSegue"])
	{
		CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
		[self.aboutMenuItem addAboutItem:aboutItem];
        
		aboutView.aboutMenuItem = self.aboutMenuItem;
		NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
	}
}
@end

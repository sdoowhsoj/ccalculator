//
//  SecondViewController.m
//  CCalc
//
//  Created by Josh Woods on 2/20/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import "SecondViewController.h"
#import "ThirdViewController.h"
#import "AboutViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController
{
    int steakServing;
    int chickenServing;
    int barbacoaServing;
    int carnitasServing;
}

-(IBAction)steakPressed
{
    if([self.steakAddButton isTouchInside])
    {
        self.steakBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        steakServing++;
        [self.menuItem addIngredientItem:self.steak];
        NSLog(@"%i", steakServing);
        if(steakServing >= 1){
            self.steakSubtractButton.hidden = NO;
        }
        self.steakSS.text = [NSString stringWithFormat:@"%i", steakServing];
        NSLog(@"%ld", (long)self.steak.nutrition.cal);
    } else if ([self.steakSubtractButton isTouchInside]){
        steakServing--;
        [self.menuItem removeIngredientItem:self.steak];
        self.steakSS.text = [NSString stringWithFormat:@"%i", steakServing];
        if(steakServing < 1)
        {
            self.steakBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.steakSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)chickenPressed
{
    if([self.chickenAddButton isTouchInside])
    {
        self.chickenBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        chickenServing++;
        [self.menuItem addIngredientItem:self.chicken];
        NSLog(@"%i", chickenServing);
        if(chickenServing >= 1){
            self.chickenSubtractButton.hidden = NO;
        }
        self.chickenSS.text = [NSString stringWithFormat:@"%i", chickenServing];
        NSLog(@"%ld", (long)self.chicken.nutrition.cal);
    } else if ([self.chickenSubtractButton isTouchInside]){
        chickenServing--;
        [self.menuItem removeIngredientItem:self.chicken];
        self.chickenSS.text = [NSString stringWithFormat:@"%i", chickenServing];
        if(chickenServing < 1)
        {
            self.chickenBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.chickenSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)carnitasPressed
{
    if([self.carnitasAddButton isTouchInside])
    {
        self.carnitasBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        carnitasServing++;
        [self.menuItem addIngredientItem:self.carnitas];
        NSLog(@"%i", carnitasServing);
        if(carnitasServing >= 1){
            self.carnitasSubtractButton.hidden = NO;
        }
        self.carnitasSS.text = [NSString stringWithFormat:@"%i", carnitasServing];
        NSLog(@"%ld", (long)self.carnitas.nutrition.cal);
    } else if ([self.carnitasSubtractButton isTouchInside]){
        carnitasServing--;
        [self.menuItem removeIngredientItem:self.carnitas];
        self.carnitasSS.text = [NSString stringWithFormat:@"%i", carnitasServing];
        if(carnitasServing < 1)
        {
            self.carnitasBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.carnitasSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)barbacoaPressed
{
    if([self.barbacoaAddButton isTouchInside])
    {
        self.barbacoaBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        barbacoaServing++;
        [self.menuItem addIngredientItem:self.barbacoa];
        NSLog(@"%i", barbacoaServing);
        if(barbacoaServing >= 1){
            self.barbacoaSubtractButton.hidden = NO;
        }
        self.barbacoaSS.text = [NSString stringWithFormat:@"%i", barbacoaServing];
        NSLog(@"%ld", (long)self.barbacoa.nutrition.cal);
    } else if ([self.barbacoaSubtractButton isTouchInside]){
        barbacoaServing--;
        [self.menuItem removeIngredientItem:self.barbacoa];
        self.barbacoaSS.text = [NSString stringWithFormat:@"%i", barbacoaServing];
        if(barbacoaServing < 1)
        {
            self.barbacoaBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.barbacoaSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)goBack
{
    [[self navigationController] popViewControllerAnimated:YES];
}

-(IBAction)startOverButtonPressed
{
    [self.menuItem.items removeAllObjects];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    self.aboutMenuItem = [[CCMenuItem alloc] init];
    
    self.steak= [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeSteak];
    self.chicken= [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeChicken];
    self.barbacoa= [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeBarbacoa];
    self.carnitas= [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeCarnitas];
    
    self.steakInfoButton.tag = CCIngredientItemTypeSteak;
    self.chickenInfoButton.tag = CCIngredientItemTypeChicken;
    self.carnitasInfoButton.tag = CCIngredientItemTypeCarnitas;
    self.barbacoaInfoButton.tag = CCIngredientItemTypeBarbacoa;
    
    self.steakSubtractButton.hidden = YES;
    self.chickenSubtractButton.hidden = YES;
    self.barbacoaSubtractButton.hidden = YES;
    self.carnitasSubtractButton.hidden = YES;

    NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    ThirdViewController *transferViewController = segue.destinationViewController;
    
    AboutViewController *aboutView = segue.destinationViewController;
    
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if([segue.identifier isEqualToString:@"secondSegue"])
    {
        transferViewController.menuItem = self.menuItem;
        NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
    } else if ([segue.identifier isEqualToString:@"steakAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"chickenAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"carnitasAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"barbacoaAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    }
}

@end

//
//  ViewController.m
//  CCalc
//
//  Created by Josh Woods on 2/9/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "AboutViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController
{
    int burritoServing;
    int hardTacoServing;
    int softTacoServing;
    int bowlServing;
}

-(IBAction)burritoPressed
{
    if([self.burritoAddButton isTouchInside])
    {
        self.burritoBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        burritoServing++;
        [self.menuItem addIngredientItem:self.burrito];
        NSLog(@"%i", burritoServing);
        if(burritoServing >= 1){
            self.burritoSubtractButton.hidden = NO;
        }
        self.burritoSS.text = [NSString stringWithFormat:@"%i", burritoServing];
        NSLog(@"%ld", (long)self.burrito.nutrition.cal);
    } else if ([self.burritoSubtractButton isTouchInside]){
        burritoServing--;
        [self.menuItem removeIngredientItem:self.burrito];
        self.burritoSS.text = [NSString stringWithFormat:@"%i", burritoServing];
        if(burritoServing < 1)
        {
            self.burritoBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.burritoSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)bowlPressed
{
    if([self.bowlAddButton isTouchInside])
    {
        self.bowlBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        bowlServing++;
        [self.menuItem addIngredientItem:self.bowl];
        NSLog(@"%i", bowlServing);
        if(bowlServing >= 1){
            self.bowlSubtractButton.hidden = NO;
        }
        self.bowlSS.text = [NSString stringWithFormat:@"%i", bowlServing];
        NSLog(@"%ld", (long)self.bowl.nutrition.cal);
    } else if ([self.bowlSubtractButton isTouchInside]){
        bowlServing--;
        [self.menuItem removeIngredientItem:self.bowl];
        self.bowlSS.text = [NSString stringWithFormat:@"%i", bowlServing];
        if(bowlServing < 1)
        {
            self.bowlBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.bowlSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)softPressed
{
    if([self.softAddButton isTouchInside])
    {
        self.softBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        softTacoServing++;
        [self.menuItem addIngredientItem:self.softTaco];
        NSLog(@"%i", softTacoServing);
        if(softTacoServing >= 1){
            self.softSubtractButton.hidden = NO;
        }
        self.softSS.text = [NSString stringWithFormat:@"%i", softTacoServing];
        NSLog(@"%ld", (long)self.softTaco.nutrition.cal);
    } else if ([self.softSubtractButton isTouchInside]){
        softTacoServing--;
        [self.menuItem removeIngredientItem:self.softTaco];
        self.softSS.text = [NSString stringWithFormat:@"%i", softTacoServing];
        if(softTacoServing < 1)
        {
            self.softBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.softSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)hardPressed
{
    if([self.hardAddButton isTouchInside])
    {
        self.hardBGLabel.backgroundColor = [UIColor colorWithRed:0.357 green:0.702 blue:0.569 alpha:1];
        hardTacoServing++;
        [self.menuItem addIngredientItem:self.hardTaco];
        NSLog(@"%i", hardTacoServing);
        if(hardTacoServing >= 1){
            self.hardSubtractButton.hidden = NO;
        }
        self.hardSS.text = [NSString stringWithFormat:@"%i", hardTacoServing];
        NSLog(@"%ld", (long)self.hardTaco.nutrition.cal);
    } else if ([self.hardSubtractButton isTouchInside]){
        hardTacoServing--;
        [self.menuItem removeIngredientItem:self.hardTaco];
        self.hardSS.text = [NSString stringWithFormat:@"%i", hardTacoServing];
        if(hardTacoServing < 1)
        {
            self.hardBGLabel.backgroundColor = [UIColor colorWithRed:0.604 green:0.733 blue:0.525 alpha:1];
            self.hardSubtractButton.hidden = YES;
        }
    }
}

-(IBAction)startOverButton
{
    [self.menuItem.items removeAllObjects];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    self.menuItem = [[CCMenuItem alloc] init];
    self.aboutMenuItem = [[CCMenuItem alloc] init];
    
    self.burrito = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeBurrito];
    self.bowl = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeBowl];
    self.hardTaco = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeHardTaco];
    self.softTaco = [CCIngredientItem ingredientItemWithType:CCIngredientItemTypeSoftTaco];

    self.burritoInfoButton.tag = CCIngredientItemTypeBurrito;
    self.bowlInfoButton.tag = CCIngredientItemTypeBowl;
    self.softTacoInfoButton.tag = CCIngredientItemTypeSoftTaco;
    self.hardInfoTacoButton.tag = CCIngredientItemTypeHardTaco;
    
    self.burritoSubtractButton.hidden = YES;
    self.bowlSubtractButton.hidden = YES;
    self.hardSubtractButton.hidden = YES;
    self.softSubtractButton.hidden = YES;
    
    NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(UIButton *)sender
{
    UINavigationController *navigationController = (UINavigationController *)self.navigationController;
    SecondViewController *transferViewController = (SecondViewController *)navigationController.topViewController;
    transferViewController = segue.destinationViewController;
    
    AboutViewController *aboutView = segue.destinationViewController;
    
    NSLog(@"prepareForSegue: %@", segue.identifier);
    if([segue.identifier isEqualToString:@"firstSegue"])
    {
        transferViewController.menuItem = self.menuItem;
        NSLog(@"%lu", (unsigned long)[self.menuItem.items count]);
    } else if ([segue.identifier isEqualToString:@"burritoAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"bowlAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"softTacoAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    } else if ([segue.identifier isEqualToString:@"hardTacoAboutSegue"])
    {
        CCIngredientItem *aboutItem = [CCIngredientItem ingredientItemWithType:sender.tag];
        [self.aboutMenuItem addAboutItem:aboutItem];
        
        aboutView.aboutMenuItem = self.aboutMenuItem;
        NSLog(@"%lu", (unsigned long)[self.aboutMenuItem.aboutItem count]);
    }
}

@end

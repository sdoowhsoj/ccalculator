//
//  CCNutrition.h
//  CCalc
//
//  Created by Josh Woods on 3/1/14.
//  Copyright (c) 2014 com.example. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CCNutrition : NSObject

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) NSInteger cal;
@property (nonatomic, assign) NSInteger tFat;
@property (nonatomic, assign) NSInteger sf;
@property (nonatomic, assign) NSInteger transf;
@property (nonatomic, assign) NSInteger cho;
@property (nonatomic, assign) NSInteger sod;
@property (nonatomic, assign) NSInteger tc;
@property (nonatomic, assign) NSInteger df;
@property (nonatomic, assign) NSInteger sug;
@property (nonatomic, assign) NSInteger pro;

@end
